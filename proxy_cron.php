<?php

$record_struct="status:retrycount:tikker:url:protocol:port:request_count:latest_respons:latest_failure:response_time:height";
$RI=array_flip(explode(":",$record_struct));

$dag=24*60*60;
$week=7*$dag;
$maand=30*$dag;

$data=file("electrumx.dat");
$data2="";
$q='{"id":1, "method":"blockchain.headers.subscribe","params":[]}';
$startall=microtime(true);

foreach ($data as $one){
	
	if ($one[0]=="#") {
		$data2.=$one;
	} elseif (trim($one)=="") {
		$data2.=$one;
	} else {
		
		$fields=explode(":",trim($one));

		$run=false;
		if (count($fields)<7) {$run=true;
		} elseif (($fields[$RI['status']]==1)||($fields[$RI['status']]==2)){$run=true;
		} elseif (($fields[$RI['status']]=='3')&&(time()-$fields[$RI['latest_failure']]>$dag)){$run=true;
		} elseif (($fields[$RI['status']]=='4')&&(time()-$fields[$RI['latest_failure']]>$week)){$run=true;
		} elseif (($fields[$RI['status']]=='5')&&(time()-$fields[$RI['latest_failure']]>$month)){$run=true;}

		if (!$run) {
			$data2.=$one;
		} else {			
			if (count($fields)<7){for ($i=7;$i<count($RI);$i++) {$fields[$i-1]=0;}}
			$fields[$RI['request_count']]++;
			$start=microtime(true);
			if (strpos($fields[$RI['url']],".cipig.")>0) { // exceptional for the time being
				$result=connectUsingTCPVerify($fields[$RI['url']],$fields[$RI['port']],$q);
			} elseif ($fields[$RI['protocol']]=="tcp"){
				$result=connectUsingTCP($fields[$RI['url']],$fields[$RI['port']],$q);
			} else {
				$result=connectUsingSSL($fields[$RI['url']],$fields[$RI['port']],$q);
			}
			$speed=ceil((microtime(true)-$start)*1000);
			$respons=false;
	
			if ($result=="") {
				// no respons
				$fields[$RI['retrycount']]++;
				if (($fields[$RI['retrycount']]>=3)&&($fields[$RI['status']]<5)) {
					$fields[$RI['status']]++;
					$fields[$RI['retrycount']]=0;
				}
				$fields[$RI['latest_failure']]=time();
			} else {
				$decode=json_decode($result,1);
				if (isset($decode['result']['height'])){
					$height=$decode['result']['height'];
					$respons=true;
				} elseif (isset($decode['result']['block_height'])){
					$height=$decode['result']['block_height'];
					$respons=true;
				} else {
					//no height
					$fields[$RI['retrycount']]++;
					if (($fields[$RI['retrycount']]>=3)&&($fields[$RI['status']]<5)) {
						$fields[$RI['status']]++;
						$fields[$RI['retrycount']]=0;
					}
					$fields[$RI['latest_failure']]=time();
				}
				if ($respons){
					$fields[$RI['status']]=1;
					$fields[$RI['retrycount']]=0;
					$fields[$RI['latest_respons']]=time();
					$fields[$RI['height']]=$height;
					$fields[$RI['response_time']]=$speed;
					$url=$fields[$RI['url']];
					$tikker=$fields[$RI['tikker']];
					$servers[$url]['tikker']=$tikker;
					$servers[$url]['response_time']=$fields[$RI['response_time']];
					$servers[$url]['height']=$height;
					$servers[$url]['spec']="$url:{$fields[$RI['port']]}:{$fields[$RI['protocol']]}";
					if (!isset($tikkers[$tikker])) { // See if any height for this tikker deviates more than one from other heights
						$tikkers[$tikker]['n']=1;
						$tikkers[$tikker]['high']=$height;
						$tikkers[$tikker]['low']=$height;
					} else {
						$tikkers[$tikker]['n']++;
						if ($tikkers[$tikker]['high']<$height) {$tikkers[$tikker]['high']=$height;}
						if ($tikkers[$tikker]['low']>$height) {$tikkers[$tikker]['low']=$height;}
					}
				}
			}
			for ($i=0;$i<count($fields);$i++) {
				if ($i!=0) {$data2.=":";}
				$data2.=$fields[$i];
			}
			$data2.="\n";
		}
	}
}
file_put_contents("electrumx.dat",$data2);
$speedall=ceil(microtime(true)-$startall);

$output="";
$output2="";
foreach ($tikkers as $tikker =>$one) {
	$best='';
	$teller=0;
	foreach ($servers as $url => $server){
		if ($server['tikker']==$tikker) {
			if (abs($one['high']-$server['height'])<=2){
				$teller++;
				if ($best=='') {$best=$url;
				} else {
					if ($servers[$url]['response_time']<$servers[$best]['response_time']){
						$best=$url;
					}
				}
			} else {
				if ($output2!="") {$output2.="\n";}
				$output2.=$url;
			}
			
		}
	}
	if ($output!="") {$output.="\n";}
	$output.="$tikker:$teller:{$servers[$best]['spec']}";
}
file_put_contents("tikkers.dat",$output);
if ($output2!=""){
	file_put_contents("electrumx_behind.dat",$output2);
} else {
	file_put_contents("electrumx_behind.dat","#");
}

function connectUsingSSL($host, $port, $query,$timeoutInSeconds=5) {
    $context = stream_context_create();
    stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
    stream_context_set_option($context, 'ssl', 'verify_peer_name', false);
	$result="";
    try {
        $socket = @stream_socket_client("ssl://$host:$port", $errno, $errstr, $timeoutInSeconds, STREAM_CLIENT_CONNECT, $context);
        if ($socket) {
            fwrite($socket, $query . "\n");
            $value = fread($socket, 81920);
            fclose($socket);
            if ($value === false) {
                $result="";
            } else {
	            $decode = json_decode($value,1);
				if ($decode === null) {
	                $result="";
				} else {
					$result=$value;
				}
            }
        }
    } catch (Exception $e) {}
	return $result;
}

function connectUsingTCP($host, $port, $query,$timeoutInSeconds=5) {
	$result="";
    try {
        $socket = @stream_socket_client("tcp://{$host}:{$port}", $errno, $errstr, $timeoutInSeconds, STREAM_CLIENT_CONNECT);
        if ($socket) {
            fwrite($socket, $query . "\n");
            $value = fread($socket, 81920);
            fclose($socket);
            if ($value === false) {
                $result="";
            } else {
				$decode = json_decode($value,1);
				if ($decode === null) {
					$result="";
				} else {
					$result=$value;
				}
			}
        } 
    } catch (Exception $e) {}
	return $result;
}

function connectUsingTCPVerify($host, $port, $query,$timeoutInSeconds=5) {
	$result="";
	$query='{"id":1,"method":"server.version","params":["ElectrumX 1.16.0", ["1.1", "1.5"]]}'."\n".$query;
    try {
        $socket = @stream_socket_client("tcp://{$host}:{$port}", $errno, $errstr, $timeoutInSeconds, STREAM_CLIENT_CONNECT);
        if ($socket) {
            fwrite($socket, $query . "\n");
			$response = ''; 
			while (!feof($socket)) {
				$chunk = fread($socket,8192);
			    if ($chunk === false) {break;}
				$response.=$chunk;
				if (substr($chunk,-2,1)=="}") {break;}
			}
            fclose($socket);
            if ($response == "") {
                $result="";
            } else {
				$value=explode("\n",$response);
				if (count($value)<2) {
					$result="";
				} else {
					$value_2=$value[1];
					$decode = json_decode($value[1],1);
					if ($decode === null) {
						$result="";
					} else {
						$result=$value[1];
					}
				}
			}
        } 
    } catch (Exception $e) {}
	return $result;
}

?>