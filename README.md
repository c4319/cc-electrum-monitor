# cc-electrum-monitor


## Motivation

The cc-electrum-monitor is a proxy service to find available and responsive electrum servers

In order to facilitate the operation of portable wallets/light clients these need to connect to the crypto networks they support. This is achieved through electrum servers. Electrum servers are indispensable but they are maintained by volunteers without being compensated. The availability of electrum servers is typically a responsability of stakeholders because the availability of light clients improves the usability and hence the adoption and value of a cryptocurrency.

## operation

This repository consists of 
- proxy_cron.php : a backend-service which typically runs once an hour to query electrum servers for the current blockheight
- electrumx.dat : a list if available electrum servers, there host and port number
- coin_response.php : a front-end monitor

If an electrumserver is not reposive it can take a while before a timeout or an invalid respose is received. Therefor a status is maintained to reduce requests from hourly to dayly, weekly and eventually monthly

Currently this service is available at https://communitycoins.org/coin_response.php
Please contribute by proposing new community coin electrum servers