<?php
$record_struct="status:retrycount:tikker:url:protocol:port:request_count:latest_respons:latest_failure:response_time:height";
$RI=array_flip(explode(":",$record_struct));

$data=file("electrumx.dat");
$behind=file("electrumx_behind.dat");

$output="";
$data2="";
$coin="";
$bootstrap='<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>';
$bootstrap.='<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">';
$bootstrap.='<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>';
$bootstrap.='<script>function klik(e) {alert(e.title)}</script>';

foreach ($data as $one){
	if ($one[0]=="#") {
		$data2.=$one;
	} elseif (trim($one)=="") {
		$data2.=$one;
	} else {
		if ($output=="") {
			$output="<html><head>$bootstrap</head><body><table class='table table-fixed'><thead style='position: sticky;top: 0'><tr >";
			$output.="<td>Freq.</td>";
			$output.="<td>Retry</td>";
			$output.="<td>Tikker</td>";
			$output.="<td>URL</td>";
			$output.="<td>Protocol</td>";
			$output.="<td>Port</td>";
			$output.="<td>N</td>";
			$output.="<td>Latest Response</td>";
			$output.="<td>Latest failure</td>";
			$output.="<td>Latency</td>";
			$output.="<td>Height</td></tr></thead><tbody>";
		}
		$fields=explode(":",trim($one));
		$status="?";
		if ($fields[$RI['status']]==1){$status="Active";
		} elseif ($fields[$RI['status']]=='2'){$status="Retry";
		} elseif ($fields[$RI['status']]=='3'){$status="Daily";
		} elseif ($fields[$RI['status']]=='4'){$status="Weekly";
		} elseif ($fields[$RI['status']]=='5'){$status="Monthly";
		}
		$fields[$RI['latest_respons']]=cv($fields[$RI['latest_respons']]);
		$fields[$RI['latest_failure']]=cv($fields[$RI['latest_failure']]);
		$exclude_style="";
		foreach ($behind as $exclude){
			if (trim($exclude)==$fields[$RI['url']]) {
				$exclude_style="text-decoration:line-through;color:red";
				break;
			}
		} 
		if ($coin==$fields[$RI['tikker']]) {
			$style="";
			if ($exclude_style!="") {$style="style='$exclude_style'";}
		} else {
			$style="style='{$exclude_style}border-top: 1px solid black'";
		}
		$coin=$fields[$RI['tikker']];
		$output.="<td $style>$status</td>";
		$output.="<td $style>{$fields[$RI['retrycount']]}</td>";
		$output.="<td $style>{$fields[$RI['tikker']]}</td>";
		$output.="<td $style title='$one' onclick='klik(this)'>{$fields[$RI['url']]}</td>";
		$output.="<td $style>{$fields[$RI['protocol']]}</td>";
		$output.="<td $style>{$fields[$RI['port']]}</td>";
		$output.="<td $style>{$fields[$RI['request_count']]}</td>";
		$output.="<td $style>{$fields[$RI['latest_respons']]}</td>";
		$output.="<td $style>{$fields[$RI['latest_failure']]}</td>";
		$output.="<td $style>{$fields[$RI['response_time']]}</td>";
		if (count($fields)>10){$output.="<td $style>{$fields[$RI['height']]}</td>";}
		$output.="</tr>";
	}
}
$output.="<tbody></table>";
$output.="</body></html>";
echo $output;

function cv($time){
	if (($time=="")||($time=="0")) {
		return("-");
	} else {
		return (date("d-m-Y H:i:s",$time));
	}
}